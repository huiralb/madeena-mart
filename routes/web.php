<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('daftar/success/{token}', 'InvestorController@success')->name('daftar.success');
Route::resource('daftar', 'InvestorController');
Route::resource('investor', 'InvestorController');

Route::resource('investor-payment', 'InvestorPaymentController');

Route::get('{page?}', 'PageController');


