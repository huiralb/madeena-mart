<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvestorsAddColumnJumlahinvest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investors', function (Blueprint $table) {
            $table->integer('jumlah_investasi')->length(255)->after('scan_ktp')->nullable();
            $table->integer('jumlah_saham')->after('jumlah_investasi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investors', function (Blueprint $table) {
            $table->dropColumn('jumlah_investasi');
            $table->dropColumn('jumlah_saham');
        });
    }
}
