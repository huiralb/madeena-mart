<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestorPayment extends Model
{
    protected $fillable = ['investor_id', 'jumlah_investasi', 'jumlah_saham', 'scan_bukti_transfer'];

    public function investor()
    {
    	return $this->belongsTo(\App\Investor::class);
    }
}
