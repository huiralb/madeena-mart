@extends('layouts.front')

@section('content')

<div class="container">
    <div class="jumbotron">
        <div class="container">
            <h1>Edit Data</h1>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('messages'))
        <div class="alert alert-success">
            <ul>
                @foreach (session('messages') as $msg)
                    <li>{!! $msg !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="{{ route('investor.update', $investor->id) }}" method="post" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{ csrf_field() }}

        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Investor</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group {{ $errors->has('no') ? 'has-error':'' }}">
                                    <label class="control-label">No:</label>
                                    <input type="text" 
                                            class="form-control" 
                                            name="no" 
                                            readonly="true" 
                                            value="{{ old('no', $investor->no) }}" />
                                </div>
                                <div class="form-group {{ $errors->has('name') ? 'has-error':'' }}">
                                    <label class="control-label">Nama:</label>
                                    <input type="text" 
                                            name="name" 
                                            class="form-control" 
                                            required
                                            value="{{ old('name', $investor->name) }}" />
                                </div>
                                <div class="form-group {{ $errors->has('hp') ? 'has-error':'' }}">
                                    <label class="control-label">HP:</label>
                                    <input type="text" 
                                            name="hp" 
                                            class="form-control"
                                            required
                                            value="{{ old('hp', $investor->hp) }}" />
                                </div>
                                <div class="form-group {{ $errors->has('alamat') ? 'has-error':'' }}">
                                    <label class="control-label">Alamat:</label>
                                    <textarea class="form-control" required name="alamat">{{ old('alamat', $investor->alamat) }}</textarea>
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error':'' }}">
                                    <label class="control-label">Email:</label>
                                    <input type="email" 
                                            name="email" 
                                            class="form-control"
                                            value="{{ old('email', $investor->email) }}" />
                                </div>
                            </div>
                                
                            <div class="col-sm-6">
                                <div class="form-group {{ $errors->has('jumlah_investasi') ? 'has-error':'' }}">
                                    <label class="control-label">Jumlah Investasi (Rp):</label>
                                    <div class="input-group">
                                        
                                        <div class="input-group-addon">Rp.</div>
                                        <input type="number" 
                                                name="jumlah_investasi"
                                                v-model="investor.jumlah_investasi"
                                                step="100000" 
                                                class="form-control">
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('jumlah_saham') ? 'has-error':'' }}">
                                    <label class="control-label">Jumlah Saham:</label>
                                    <div class="input-group">
                                        <input type="number" 
                                                name="jumlah_saham" 
                                                readonly 
                                                v-model="investor.jumlah_saham"
                                                class="form-control">
                                        <div class="input-group-addon">Lembar</div>
                                        
                                    </div>
                                </div>

                                <hr/>

                                <div class="form-group {{ $errors->has('no_ktp') ? 'has-error':'' }}">
                                    <label class="control-label">NO KTP:</label>
                                    <input type="text" 
                                            name="no_ktp" 
                                            class="form-control"
                                            required 
                                            value="{{ old('no_ktp', $investor->no_ktp) }}" />
                                </div>

                                <div class="form-group {{ $errors->has('scan_ktp') ? 'has-error':'' }}">
                                    <label class="control-label">{{ $investor->scan_ktp ? 'Ganti' : 'Upload' }} Scan KTP:</label>
                                    <input type="file" name="scan_ktp" />
                                </div>
                                
                                @if($investor->scan_ktp)
                                <div class="form-group">
                                    <img src="{{ url('img/upload', $investor->scan_ktp) }}" class="img-responsive">
                                </div>
                                @endif
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">
                            <i class="fa fa-save"></i>
                            Update
                        </button>
                        <a href="{{ route('investor.index') }}" class="btn btn-link">Cancel</a>
                    </div>
                </div>
                
            </div>
        </div>
    </form>

    <form id="delete" action="{{ route('investor.destroy', $investor->id) }}" method="DELETE">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <button class="btn btn-danger" type="submit">
                            <span class="glyphicon glyphicon-trash"></span> 
                            Hapus
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection

@push('scripts')
    <script>
    new Vue({
        el: '#app',
        data: {
            investor: {
                jumlah_investasi: '{{ old('jumlah_investasi', $investor->jumlah_investasi) }}',
                jumlah_saham: '{{ old('jumlah_saham', $investor->jumlah_saham) }}',
            },
            // harga saham perlembar
            harga: 100000,
        },
        watch: {
            investor: {
                handler(value){
                    var invs = value.jumlah_investasi;
                    if(!invs) return;

                    if(invs%this.harga != 0)
                    {
                        toastr.warning('Jumlah investasi kelipatan 100.000');
                        this.investor.jumlah_investasi = value.jumlah_saham*this.harga;
                        return false;
                    }

                    this.investor.jumlah_saham = parseInt(invs)/this.harga;
                },
                deep: true
            }
        }
    })
    var $form = $('#delete');

        $form.submit(function(e){

            if(!confirm('Yakin Hapus ?')) return false;

            var data = {
                _method: $form.attr('method')
            }

            axios.post($form.attr('action'), data)
                    .then(function(res){
                        toastr.success('Hapus Berhasil!')
                        setTimeout(function(){
                            window.location.href = res.data.url
                        }, 2000)

                    })

            e.preventDefault()
        })
    </script>
@endpush
