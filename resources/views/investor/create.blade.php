@extends('layouts.front')

@section('content')

	<div class="jumbotron">
		<div class="container">
			<h1>Daftar Investor</h1>
			<p>Daftar menjadi investor</p>
		</div>
	</div>
	
	<div class="container">
		<form action="{{ route('daftar.store') }}" method="post" enctype="multipart/form-data">
		
			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			@if (session('errors'))
			    <div class="alert alert-danger">
			        <ul>
			            @foreach (session('errors') as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif

			<div class="row">
				<div class="col-sm-6">
						{{ csrf_field() }}
					    <div class="panel panel-default">
					        <div class="panel-heading">
					            <h3 class="panel-title">Mohon isikan form berikut</h3>
					        </div>
					        <div class="panel-body">
					        	<div class="row">
					        		<div class="col-sm-6">
							            <div class="form-group {{ $errors->has('no') ? 'has-error':'' }}">
							            	<label class="control-label">No:</label>
							            	<input type="text" 
							            			class="form-control" required 
							            			name="no" 
							            			readonly="true" 
							            			value="{{ $no }}" />
							            </div>
							            <div class="form-group {{ $errors->has('name') ? 'has-error':'' }}">
							            	<label class="control-label">Nama:</label>
							            	<input type="text" 
							            			name="name" 
							            			class="form-control" 
							            			value="{{ old('name') }}"
							            			required />
							            </div>
							            <div class="form-group {{ $errors->has('hp') ? 'has-error':'' }}">
							            	<label class="control-label">HP:</label>
							            	<input type="text" 
							            			name="hp" 
							            			value="{{ old('hp') }}"
							            			class="form-control" required />
							            </div>
							            <div class="form-group {{ $errors->has('alamat') ? 'has-error':'' }}">
							            	<label class="control-label">Alamat:</label>
							            	<textarea class="form-control" required name="alamat">{{ old('alamat') }}</textarea>
							            </div>
							            <div class="form-group {{ $errors->has('email') ? 'has-error':'' }}">
							            	<label class="control-label">Email:</label>
							            	<input type="email" name="email" value="{{ old('email') }}" class="form-control" />
							            </div>
					        		</div>
					        			
					        		<div class="col-sm-6">
							            <div class="form-group {{ $errors->has('jumlah_investasi') ? 'has-error':'' }}">
							            	<label class="control-label">Investasi:</label>
							            	<div class="input-group">
							            		<div class="input-group-addon">Rp.</div>
							            		<input type="number" 
							            				step="100000"  
							            				min="100000"  
							            				name="jumlah_investasi" 
							            				v-model="investor.jumlah_investasi" 
							            				class="form-control" 
							            				value=""
							            				required />
							            	</div>
							            </div>
							            <div class="form-group {{ $errors->has('jumlah_saham') ? 'has-error':'' }}">
							            	<label class="control-label">Saham:</label>
							            	<div class="input-group">
							            		<input type="jumlah_saham" 
							            				name="jumlah_saham" 
							            				readonly 
							            				v-model="investor.jumlah_saham" 
							            				class="form-control" 
							            				value="" />
							            		<div class="input-group-addon">Lembar</div>
							            	</div>
							            </div>

							            <hr/>

				                        <div class="form-group {{ $errors->has('no_ktp') ? 'has-error':'' }}">
				                            <label class="control-label">NO KTP:</label>
				                            <input type="text" 
				                                    name="no_ktp" 
				                                    class="form-control"
				                                    required 
				                                    value="{{ old('no_ktp') }}" />
				                        </div>

				                        <div class="form-group {{ $errors->has('scan_ktp') ? 'has-error':'' }}">
				                            <label class="control-label">Upload Scan KTP:</label>
				                            <input type="file" name="scan_ktp" id="scan_ktp" required="required" />
				                        </div>

				                        <div class="form-group">
				                        	<div class="checkbox">
				                        		<label>
				                        			<input type="checkbox" name="skip_upload" v-model="input.skip_upload">
				                        			Lewati upload scan KTP
				                        		</label>
				                        	</div>
				                        </div>
					        		</div>

					        	</div>
					        </div>
					    </div>
					
				</div>
				<div class="col-sm-6">
					<h3>Penting diperhatikan !</h3>
					<p>Saya mengerti dan paham bahwa dengan menjadi bagian dari Madeena Mart, saya berkomitmen untuk belanja di toko Madeena Mart dengan minimal belanja senilai:</p>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<select name="min_belanja" 
										v-model="input.min_belanja" 
										@change="onSelectMinBelanja" 
										required 
										class="form-control">
									@foreach($min_belanja as $key => $min)
									<option value="{{ $min*1000 }}">{{ $min }} Ribu</option>
									@endforeach
									<option value="0">1Juta ke atas</option>
								</select>
								<p class="text-muted" style="font-size: 12px; font-style: italic;">Pilih minimal nilai belanja</p>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group" v-if="input.opt_min_belanja != 0">
								<input type="number" 
										:step="harga" 
										:min="10*harga" 
										name="opt_min_belanja" 
										v-model="input.opt_min_belanja" 
										required  
										class="form-control">
								<p class="text-muted" style="font-size: 12px; font-style: italic;">Masukkan nilai belanja 1juta ke atas</p>
							</div>
						</div>
					</div>


					<div class="form-group">
						<button type="submit" class="btn btn-primary">Daftar</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	
@endsection

@push('scripts')
<script>
	new Vue({
		el: '#app',
		data: {
			investor: {
				jumlah_investasi: '{{ old('jumlah_investasi', 1000000) }}',
				jumlah_saham: '{{ old('jumlah_saham', 10) }}',
			},
			// harga saham perlembar
			harga: 100000,
			input: {
				scan_ktp: false,
				skip_upload: false,
				min_belanja: '{{ old('min_belanja', 300*1000) }}',
				opt_min_belanja: 0,
			}
		},
		watch: {
			investor: {
				handler(value){
					var invs = value.jumlah_investasi;
					if(!invs) return;

					if(invs%this.harga != 0)
					{
						toastr.warning('Jumlah investasi kelipatan '.this.harga);
						this.investor.jumlah_investasi = value.jumlah_saham*this.harga;
						return false;
					}

					this.investor.jumlah_saham = parseInt(invs)/this.harga;
				},
				deep: true
			},
			input: {
				handler(value){
					var scanKtp = document.querySelector('#scan_ktp')
					var opt_min = value.opt_min_belanja

					if(!value.skip_upload)
					{
						scanKtp.setAttribute('required', 'required')
					}
					else{
						scanKtp.removeAttribute('required')
					}

					if(opt_min != 0 && opt_min < 1000*1000)
					{
						this.input.opt_min_belanja = 1000000;
						toastr.warning('Nilai belanja minimal 1Juta')
					}
				},
				deep: true
			}
		},
		methods: {
			onSelectMinBelanja(e){
				if(e.target.value == 0)
				{
					this.input.opt_min_belanja = 1000*1000;
				}
				else{
					this.input.opt_min_belanja = 0;
				}
			}
		}
	})
</script>
@endpush
