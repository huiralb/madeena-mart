<?php

namespace App\Http\Controllers;

use App\Investor;
use Illuminate\Http\Request;

class InvestorPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $investors = Investor::with('payment')->get();

        if($request->ajax() && $request->ajax)
        {
            return $investors;
        }

        return view('investor-payment.index', compact('investors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filename = str_random(32).'.jpg';

        $image = \Image::make($request->scan_bukti_transfer);

        $image->save(storage_path('app/img/upload/').$filename, 100);

        $payment = Investor::find($id)->payment;

        $payment->update(array_merge($request->all(), ['scan_bukti_transfer' => $filename]));

        return $payment;

    }

    /**
     * view pdf with dompdf
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $terbilang = new \Nasution\Terbilang();
        $investor = Investor::findOrFail($id);
        $investor->terbilang = $terbilang->convert($investor->payment->jumlah_investasi);

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('investor-payment.pdf-kwitansi', compact('investor'));
        $pdf->setPaper('a5', 'landscape');
        return $pdf->stream();
    }
}
