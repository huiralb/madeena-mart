@extends('layouts.front')

@section('content')
	<section id="hero">
	    <div class="jumbotron hero section">
	        <div class="container text-center">
	            <h2>Selamat datang di MADEENA INDONESIA</h2>
	            <p>Mari bergabung bersama kami</p>
	            <p>
	                <a href="{{ route('daftar.create') }}" class="btn btn-warning btn-lg">Daftar</a>
	            </p>
	        </div>
	    </div>
	</section>

@endsection