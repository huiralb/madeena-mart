<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __invoke($page = 'index')
    {
        $page = camel_case($page);

        if (!method_exists($this, $page)) {
            return abort(404);
        }

        return call_user_func([$this, $page]);

    }

    public function index()
    {
    	return view('page.index');
    }
}
