@extends('layouts.front')

@section('content')
	<h2>&nbsp;</h2>
	<div class="container text-center">
		<div class="alert alert-success">
			Pendaftaran Berhasil.
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<h1>Selamat! sebentar lagi anda terdaftar menjadi investor</h1>
				<p>Untuk proses pengesahan, silahkan transfer dana Rp. <h3 class="text-danger"> {{ $investor->jumlah_investasi ? number_format($investor->jumlah_investasi) : '--' }}</h3> ke:</p>

				<p>
					Bank Syariah Mandiri<br/>
					7-1111-499-89<br/>
					PT. SOUQ MADEENA INDONESIA
				</p>
			</div>
		</div>

		<p style="margin-bottom: 26px">
			<a href="{{ url('/') }}" class="btn btn-default">
				<span class="glyphicon glyphicon-home"></span> 
				Ke Halaman utama
			</a>
		</p>

	</div>
@endsection
