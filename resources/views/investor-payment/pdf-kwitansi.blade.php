<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>

    <title>Kwitansi #KW-{{ $investor->no }}</title>
    <style>
    	{{ file_get_contents( base_path('public/css/app.css')) }}
        body {
            background-color: #fff;
        }
        .table td {
            vertical-align: top;
        }
        .table.detail {
            border: 2px solid #ddd;
        }

        .table.detail td {
            padding: 10px;
        }
        .space{
            margin: 30px 0;
        }
    </style>
</head>
<body>
    <table class="table">
        <tr>
            <td style="width: 40%; border-bottom: 1px solid #ddd;">
    	       <img src="{{ asset('img/logo.png') }}" style="height:50px"/>
            </td>
            <td style="width: 50%; border-bottom: 1px solid #ddd">
                <h4>PT Souq Madeena Indonesia</h4>
                <p>
                    Jl. Tegalrejo Raya No. 5 RT. 02 RW. 05<br/>
                    Kel. Tegalrejo Kec. Argomulyo Salatiga
                </p>
                
            </td>
            <td style="width: 30%; border-bottom: 1px solid #ddd">
                <h3 class="text-right">KWITANSI</h3>
                <p class="text-right">
                    No. KW-{{ $investor->no }}
                </p>
            </td>
        </tr>
    </table>
    <table class="table detail">
        <tbody>
            <tr>
                <td style="width: 30%">Telah diterima dari</td>
                <td style="width: 2%">:</td>
                <td style="width: 89%">
                    <span class="text-uppercase">{{ $investor->name }}</span> - {{ $investor->alamat }}
                </td>
            </tr>
            <tr>
                <td style="width: 30%">Banyaknya Uang:</td>
                <td style="width: 2%">:</td>
                <td style="width: 89%" class="text-capitalize">{{ $investor->terbilang }} Rupiah</td>
            </tr>
            <tr>
                <td style="width: 30%">Guna pembayaran:</td>
                <td style="width: 2%">:</td>
                <td style="width: 89%">Saham sebanyak {{ $investor->jumlah_saham }} lembar</td>
            </tr>
        </tbody>
    </table>

    <table class="table">
        <tbody>
            <tr>
                <td style="width: 60%">
                    <h2>Rp. {{ $investor->jumlah_investasi ? number_format($investor->jumlah_investasi) : '--' }}</h2>
                </td>
                <td style="width: 90%; text-align: right">
                    <p class="text-right">
                        Salatiga, {{ date('d M Y') }}
                    </p>
                    <p class="space">&nbsp;</p>

                    <h5>Hurip Indriyono</h5>
                    <h5>--Direktur Keuangan--</h5>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
