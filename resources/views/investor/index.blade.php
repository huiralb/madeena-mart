@extends('layouts.front')

@push('styles')
	<style>
		.menu {
			margin-top: 3.4em
		}
	</style>
@endpush

@section('content')
	@if (session('messages'))
        <div class="alert alert-success">
            <ul>
                @foreach (session('messages') as $msg)
                    <li>{!! $msg !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
	<p class="text-right">
		<a href="{{ route('investor.create') }}" class="btn btn-success">
		<span class="glyphicon glyphicon-plus"></span>Daftar Investor</a>
	</p>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Investor</h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered">
            	<thead>
            		<tr>
            			@foreach($columns as $column)
            				<th>{!! $column !!}</th>
            			@endforeach
            		</tr>
            	</thead>
            	<tbody>
            		@if($investors->count())
            			@foreach($investors as $investor)
            			<tr>
            				<td>{{ $investor->no }}</td>
            				<td>{{ $investor->name }}</td>
            				<td>{{ $investor->alamat }}</td>
            				<td>{{ $investor->hp }}</td>
            				<td>Rp. {{ $investor->jumlah_investasi ? number_format($investor->jumlah_investasi) : 0 }}</td>
            				<td>{{ $investor->jumlah_saham }}</td>
            				<td>
            					<a href="{{ route('investor.edit', $investor->id)}}" class="btn btn-primary btn-sm">
            						Detail
            					</a>
            				</td>
            			</tr>
            			@endforeach
            		@endif
            	</tbody>
            </table>
        </div>
        <div class="panel-footer">
			@if($investors->count())
				{{ $investors->links() }}
			@else
				<p class="text-muted">Data Kosong</p>
			@endif
        </div>
    </div>
@endsection
