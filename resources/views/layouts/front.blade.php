<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Madeena Mart') }}</title>

    @stack('styles')
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        [v-cloak]{ display:none }
        #navbar {
            margin-bottom: 0;
        }

        main#content {
            min-height: 491px;
        }

        footer {
            padding: 30px;
            text-align: center;
            background-color: #282d35;
            color: white;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav id="navbar" class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <span class="sr-only">{{ config('app.name', 'Madeena Mart') }}</span>
                        <img src="{{ url('img/logo.png') }}" class="img-responsive">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                    </ul>
                </div>
            </div>
        </nav>
        
        <main id="content">
            @yield('content')
        </main>   

        <footer id="footer">
            Madeena Indonesia
        </footer>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    @stack('scripts')
    
</body>
</html>
