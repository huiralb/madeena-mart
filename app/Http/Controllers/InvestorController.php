<?php

namespace App\Http\Controllers;

use App\Investor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvestorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * List
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        return redirect()->route('daftar.create');

        $data = [
            'investors' => Investor::paginate(10),
            'columns'   => ['No', 'Nama', 'Alamat', 'Hp', 'Investasi (Rp) <br/> (Planing)', 'Saham', '&nbsp;']
        ];

    	return view('investor.index', $data);
    }

    /**
     * Form Tambah
     * @return [type] [description]
     */
    public function create()
    {
        $no = $this->generateNo();

        $min_belanja = $this->rangeBelanja();

    	return view('investor.create', compact('no', 'min_belanja'));
    }

    /**
     * Post form tambah
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $inputs = [];
        $this->validate($request, [
                'no'                => 'required',
                'name'              => 'required',
                'alamat'            => 'required',
                'hp'                => 'required',
                'jumlah_investasi'  => 'required',
                'jumlah_saham'      => 'required',
                'min_belanja'       => 'required',
            ]);

        # upload scan ktp
        if($request->hasFile('scan_ktp'))
        {
            $file = $this->storeScanPhoto($request->file('scan_ktp'));

            $inputs['scan_ktp'] = $request->scan_ktp->isValid() && $file ? $file : '';
        }

        if($request->opt_min_belanja && $request->opt_min_belanja >= 1000*1000)
        {
            $inputs['min_belanja'] = $request->opt_min_belanja;
        }

        $inputs['token'] = 1;

        $inputs = array_merge($request->except(['_method', '_token']), $inputs);

        $investor = Investor::create($inputs);

        $investor->payment()->create([]);

    	if($investor)
    	{
            $messages = [
                    'Berhasil daftar investor',
                    'Lengkapi data <strong>NO KTP</strong> dan <strong>Upload Scan KTP</strong>'
                ];
    		return redirect()
                    ->route('daftar.success', $investor->token)
                    ->with('messages', $messages);
    	}

    	return redirect()->back()->with('errors', ['Gagal, ada suatu masalah']);
    }

    /**
     * Form Edit
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
    	$investor = Investor::findOrFail($id);

    	return view('investor.edit', compact('investor'));
    }

    /**
     * PUT form edit
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'no'        => 'required',
                'name'      => 'required',
                'alamat'    => 'required',
                'hp'        => 'required',
                'no_ktp'    => 'required'
            ]);

        $inputs = [];
        # upload scan ktp
        if($request->hasFile('scan_ktp'))
        {
            $file = $this->storeScanPhoto($request->file('scan_ktp'));

            $inputs['scan_ktp'] = $request->scan_ktp->isValid() && $file ? $file : '';
        }

        $inputs['email'] = is_null($request->email) ? '' : $request->email;

        $inputs = array_merge($request->except(['_method', '_token']), $inputs);

        Investor::where('id', $id)->update( $inputs );

        return redirect()->route('daftar.edit', $id);
    }

    public function destroy($id)
    {
        $investor = Investor::findOrFail($id);

        $ktp = storage_path('app/img/upload/'.$investor->scan_ktp);

        DB::beginTransaction();

        if($investor->delete())
        {
            if(file_exists(($ktp)))
            {
                unlink($ktp);
            }
        }

        DB::commit();

        session('messages', ['Berhasil hapus !']);

        return json_encode(['url' => route('investor.index')]);
    }

    public function success(Request $request, $token)
    {
        $investor = Investor::where('token')->first();

        return view('investor.success', compact('investor'));
    }

    protected function storeScanPhoto($file)
    {
        $ext = $file->extension();

        if(!in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'bitmap']))
        {
            return false;
        }

        $path = $file->store('img/upload');

        $files = explode('/', $path);

        return end($files);
    }

    protected function generateNo()
    {
        return Investor::lastNo();
    }

    protected function rangeBelanja()
    {
        return range(300, 900, 100);
    }
}
