<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
    protected $fillable = [
    	'no',
    	'name',
    	'alamat',
    	'hp',
    	'email',
    	'no_ktp',
    	'scan_ktp',
    	'jumlah_investasi',
        'jumlah_saham',
        'min_belanja',
    	'token', 
    ];

    public function payment()
    {
    	return $this->hasOne(\App\InvestorPayment::class);
    }

    public function setNoAttribute($value)
    {
        $this->attributes['no'] = $this->lastNo();
    }

    public function setTokenAttribute($value)
    {
        $this->attributes['token'] = str_random(64);
    }

    public static function lastNo()
    {
        $no = '0001';

        $investor = self::orderBy('id', 'desc')->take(1)->first();

        if($investor)
        {
            $no = sprintf('%04d', (int)$investor->no +1);
        }

        return $no;
    }
}
