<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvestorPaymentsAddJumlahSaham extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_payments', function (Blueprint $table) {
            $table->integer('jumlah_saham')->nullable()->after('jumlah_investasi');
            $table->integer('jumlah_investasi')->nullable()->change();
            $table->string('scan_bukti_transfer')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_payments', function (Blueprint $table) {
            $table->dropColumn('jumlah_saham');
            $table->integer('jumlah_investasi')->change();
            $table->string('scan_bukti_transfer')->change();
        });
    }
}
