@extends('layouts.app')

@section('content')
	<div v-cloak="">
		
		<div class="panel panel-default" v-if="!investor">
			<div class="panel-body">
				<table class="table">
					<thead>
						<tr>
							<th>NO</th>
							<th>Nama</th>
							<th>Investasi <br/>(yang sudah dibayarkan)</th>
							<th>Saham</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="investor in investors">
							<td>@{{ investor.no }}</td>
							<td>@{{ investor.name }}</td>
							<td>@{{ investor.payment.jumlah_investasi }}</td>
							<td>@{{ investor.payment.jumlah_saham }}</td>
							<td>
								<button class="btn btn-default" @click="openForm(investor)">Detail</button>
								<a :href="'/investor-payment/'+investor.id" class="btn btn-default">
									Cetak kwitansi
								</a>
							</td>
						</tr>
					</tbody>
				</table>
				
			</div>
		</div>
		
		<div class="panel panel-primary" v-if="investor">
			<div class="panel-heading">
				<h3 class="panel-title">Form Setoran</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label>Jumlah Investasi:</label>
							<input type="text" class="form-control" v-model="investor.jumlah_investasi" />
						</div>
						<div class="form-group">
							<label>Jumlah Saham:</label>
							<input type="text" class="form-control" v-model="investor.jumlah_saham" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group" v-if="!image">
							<label>Upload Scan bukti transfer:</label>
							<input type="file" class="form-control" @change="uploadScanTransfer">
						</div>

						<div class="form-group" v-else>
							<div class="image">
								<img :src="image" class="img-responsive">
							</div>
							<button class="btn btn-sm btn-default" @click="deleteImage">
								<span>Ganti</span>
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<button class="btn btn-default" @click="back">
					Kembali
				</button>
				<button class="btn btn-primary pull-right" @click="submit">Simpan</button>
			</div>
		</div>

	</div>
@endsection

@push('scripts')
<script>
	new Vue({
		el: '#app',
		data: {
			investors: null,
			investor: null,
			query: null,
			image: null,
		},
		beforeMount(){
			this.init();
		},
		methods: {
			init () {
				var self = this;
				axios.get('/investor-payment?ajax=1')
					.then(function(res){
						self.investors = res.data
					});
			},
			openForm(investor){
				this.investor = {
					id: investor.id,
					jumlah_investasi: investor.jumlah_investasi,
					jumlah_saham: investor.jumlah_saham
				}
				this.selectedInvestors = null;
				this.image = '/img/upload/'+investor.payment.scan_bukti_transfer;
			},
			back(){
				this.investor = null;
			},
			uploadScanTransfer(e){
				var files = e.target.files || e.dataTransfer.files
				if(!files) return;
				this.createImage(files[0])
			},
			createImage(file){
				var image = new Image();
			    var reader = new FileReader();
			    var vm = this;

			    reader.onload = (e) => {
			    	vm.image = e.target.result;
			    };

			    vm.image = file.name
			    reader.readAsDataURL(file);
			},
			deleteImage(e) {
				this.image = null;
			},
			submit(){
				var data = {
					jumlah_saham: this.investor.jumlah_saham,
					jumlah_investasi: this.investor.jumlah_investasi,
					scan_bukti_transfer: this.image,
				}

				axios.put('/investor-payment/'+this.investor.id, data)
					.then(function(res){
						toastr.success('Berhasil!')
						setTimeout(function(){
							window.location.reload()
						}, 1000)
					})
			}
		}
	});
</script>
@endpush